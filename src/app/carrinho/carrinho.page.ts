import { Component, OnInit } from '@angular/core';
import { Bolo } from '../home/bolo';
import { BoloService } from '../bolo.service';
import { LoadingController, AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.page.html',
  styleUrls: ['./carrinho.page.scss'],
})
export class CarrinhoPage implements OnInit {

  carrinho: Bolo[] = [];
  total: number = 0;

  constructor(private boloService: BoloService, private loadingController: LoadingController, private alertController: AlertController, private navController: NavController) { }

  ionViewDidEnter() {
    this.carrinho = this.boloService.getCarrinho();
    this.carrinho.forEach(bolo => {
      this.total += bolo.preco;
    })
  }

  ngOnInit() { }

  async fecharCompra() {

    const alert = await this.alertController.create({
      header: "Compra Realizada com Sucesso!",
      message: "Obrigado pela Preferência!",
      buttons: [
        {
          text: "OK"
        }
      ]
    });

    const loading = await this.loadingController.create({
      message: "Finalizando Compra...",
      spinner: "bubbles",
      translucent: true,
      duration: 2000
    });

    loading.present();

    loading.onDidDismiss().then(d => {
      alert.present();
    });

    alert.onDidDismiss().then(d => {
      this.navController.pop();
    });
  }

}
