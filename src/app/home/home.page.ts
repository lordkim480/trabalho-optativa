import { Component } from '@angular/core';
import { BoloService } from '../bolo.service';
import { Bolo } from './bolo';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  bolos: Bolo[] = [];
  carrinho: Bolo[] = [];

  constructor(private boloService: BoloService, private navController: NavController) {
    this.boloService.getBolos().subscribe(json => {
      this.bolos = json['bolos'];
    });
    this.carrinho = this.boloService.getCarrinho();
  }

  ionViewDidEnter() {
    console.log(this.carrinho.length);
  }

  detalhes(bolo: Bolo) {
    let extras: NavigationExtras = {
      state: {
        bolo: bolo
      }
    };
    this.navController.navigateForward('/detalhe-bolo', extras);
  }

  abrirCarrinho() {
    this.navController.navigateForward('/carrinho');
  }

}
