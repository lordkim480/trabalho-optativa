import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Bolo } from '../home/bolo';
import { NavController, AlertController } from '@ionic/angular';
import { BoloService } from '../bolo.service';

@Component({
  selector: 'app-detalhe-bolo',
  templateUrl: './detalhe-bolo.page.html',
  styleUrls: ['./detalhe-bolo.page.scss'],
})
export class DetalheBoloPage implements OnInit {

  state: any;
  bolo: Bolo;

  constructor(private router: Router, private navController: NavController, private boloService: BoloService, private alertContoller: AlertController) {
    this.state = this.router.getCurrentNavigation().extras.state;
    if (this.state != undefined) {
      this.bolo = this.state['bolo'];
    }
  }

  ngOnInit() {
  }

  async addCarrinho(bolo: Bolo) {
    this.boloService.adcionarCarrinho(bolo);
    const alert = await this.alertContoller.create({
      message: bolo.nome + " foi adicionado ao carrinho",
      buttons: [
        {
          text: "Ok"
        }
      ]
    });
    alert.present();
    this.navController.pop();
  }

}
